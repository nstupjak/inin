﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySimpleApp.Models
{
    public class User
    {
        public string Employee { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Date { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        public float Sum { get; set; }

    }
}